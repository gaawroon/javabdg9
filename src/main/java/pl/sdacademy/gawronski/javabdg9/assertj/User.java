package pl.sdacademy.gawronski.javabdg9.assertj;

import java.util.Objects;

public class User {
    private String name;
    private int age;
    private Address address;

    public User() {}

    public User(String name) {
        this.name = name;
        this.age = 0;
        this.address = new Address("Bydgoszcz", "10-948");
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Address getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return age == user.age &&
                Objects.equals(name, user.name) &&
                Objects.equals(address, user.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, address);
    }
}
