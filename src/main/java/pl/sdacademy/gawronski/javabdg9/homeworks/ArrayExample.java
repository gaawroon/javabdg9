package pl.sdacademy.gawronski.javabdg9.homeworks;

import java.util.ArrayList;
import java.util.List;

public class ArrayExample {

    public static List<String> removeDuplicates(List<String> array) {
        if(array == null) {
            return null;
        }

        List<String> result = new ArrayList<>();

        for(String string : array) {
            if(!result.contains(string)) {
                result.add(string);
            }
        }

        return result;
    }
}
