package pl.sdacademy.gawronski.javabdg9.homeworks.books;

import java.util.List;

public class Basket {
    private List<Book> books;

    public Basket(List<Book> books) {
        this.books = books;
    }

    public void addBook(Book book) {
        if (book != null) {
            books.add(book);
        }
    }

    public void clearBasket() {
        books.clear();
    }
}
