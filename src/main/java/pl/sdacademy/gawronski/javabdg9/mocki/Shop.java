package pl.sdacademy.gawronski.javabdg9.mocki;

import java.util.List;

public class Shop {

    private ItemRepository itemRepository;

    public Shop(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public float sumPrices(List<String> ids) {
        float sum = 0;
        for(String id : ids) {
            try {
                Item item = itemRepository.getItem(id);
                sum += item.getPrice();
            } catch (Exception e) {
                System.out.println("Polecial wyjatek!");
            }
        }
        return sum;
    }

}
