package pl.sdacademy.gawronski.javabdg9;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

class CalculatorAssertJTest {

    private Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @Test
    void shouldThrowExceptionWhenDivisionBy0() { // AssertJ
        // given
        int a = 10;
        int b = 0;

        // when/then
        assertThatThrownBy(
                () -> calculator.divide(a, b)
        ).isInstanceOf(IllegalArgumentException.class);
    }
}