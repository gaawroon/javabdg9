package pl.sdacademy.gawronski.javabdg9;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.NullString;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorParametrizedTest {

    private Calculator calculator;

    @BeforeEach
    void setUp() {
        System.out.println("Tworze kalkulator");
        calculator = new Calculator();
    }

    @ParameterizedTest
    @CsvSource({
            "4, 7, 11",
            "8, 13, 21"
    })
    void shouldAddTwoNumbers(int a, int b, int sum) {
        // given
        // pusty

        // when
        int result = calculator.add(a, b);

        // then
        assertEquals(sum, result);
    }

    @ParameterizedTest
    @ValueSource(ints = {5, 0, -40})
    void shouldAlwaysThrowIllegalArgumentExceptionWhenDivisionBy0(int a) {
        // given
        int b = 0;

        // when
        assertThrows(
                IllegalArgumentException.class,
                () -> calculator.divide(a, b)
        );
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/test.csv")
//    @MethodSource
//    @EnumSource
//    @EmptySource
//    @NullAndEmptySource
//    @NullSource
//    @NullString()
    void shouldAddTwoNumbersFromCsv(int a, int b, int sum) {
        // given
        // pusty

        // when
        int result = calculator.add(a, b);

        // then
        assertEquals(sum, result);
    }

}
