package pl.sdacademy.gawronski.javabdg9;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;


class CalculatorTest {

    private Calculator calculator;

    @BeforeEach  // metoda odpali sie przed kazdym pojedynczym testem z tej klasy
    void setUp() {
        System.out.println("Tworze nowy kalkulator");
        calculator = new Calculator();
    }

    @Test
    void mojPierwszyTest() {
    }

    @Test
    void testAddUjemnych() {
        // given
        int a = -3;
        int b = -16;

        // when
        int result = calculator.add(a, b);

        // then
        assertEquals(-19, result);
    }

    @Test
    void testAdd() {
        // given
        int a = 3;
        int b = 8;

        // when
        int result = calculator.add(a, b);

        // then
        assertEquals(11, result);       // metoda statyczna z JUnit

/*        if(result != 11) {
            fail();
        }
        */
    }

    @Test
    void testDzielenia() {
        // given
        int a = 3;
        int b = 10;

        // when
        double result = calculator.divide(a, b);

        // then
        assertEquals(0.3, result);
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenDivisionBy0() {     // reczne sprawdzenie
        // given
        int a = 10;
        int b = 0;

        // when
        Exception e = null;
        try {
            calculator.divide(a, b);
        } catch(IllegalArgumentException exception) {
            e = exception;
        }

        // then
        assertNotNull(e);
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenDivisionBy0JUnit() { // sprawdzenie z JUNit
        // given
        int a = 10;
        int b = 0;

        // when
        assertThrows(
                IllegalArgumentException.class,
                () -> calculator.divide(a, b)
        );
    }
}