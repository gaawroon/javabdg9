package pl.sdacademy.gawronski.javabdg9.assertj;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UserTest {

    // ponizsze testy robia to samo - sprawdzaja wszystkie pola 'Usera'

    @Test
    void shouldCreateProperUser() {
        // given
        // puste

        // when
        User u = new User("Mariusz");

        // then
        assertThat(u.getName()).isEqualTo("Mariusz");
        assertThat(u.getAge()).isEqualTo(0);
        assertThat(u.getAddress().getCity()).isEqualTo("Bydgoszcz");
        assertThat(u.getAddress().getPostalCode()).isEqualTo("10-948");

        // nie da sie:
//        assertThat(u).hasAddress...
    }

    @Test
    void shouldCreateProperUser2() {
        // given
        Address expectedAddress = new Address();
        expectedAddress.setCity("Bydgoszcz");
        expectedAddress.setPostalCode("10-948");

        User expected = new User();
        expected.setName("Mariusz");
        expected.setAge(0);
        expected.setAddress(expectedAddress);

        // when
        User u = new User("Mariusz");

        // then
        assertThat(u).isEqualTo(expected);
    }

}