package pl.sdacademy.gawronski.javabdg9.homeworks;


import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ArrayExampleTest {
//    powinna zwrócić nową listę bez duplikatów

    @Test
    void shouldHandleOneElementList() {
        // given
        List<String> list = new ArrayList<>();
        list.add("1");

        // when
        List<String> result = ArrayExample.removeDuplicates(list);

        // expect
        assertThat(result)
                .hasSize(1)
                .containsOnlyOnce("1");
    }

    @Test
    void shouldRemoveDuplicates() {
        // given
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("3");

        // when
        List<String> result = ArrayExample.removeDuplicates(list);

        // expect
        assertThat(result)
                .hasSize(3)
                .containsOnlyOnce("1")
                .containsOnlyOnce("2")
                .containsOnlyOnce("3");
    }

    @Test
    void shouldHandleNullValues() {
        // given
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add(null);
        list.add(null);

        // when
        List<String> result = ArrayExample.removeDuplicates(list);

        // expect
        assertThat(result)
                .hasSize(2)
                .containsOnlyOnce("1")
                .containsNull();
    }

    @Test
    void shouldHandleEmptyList() {
        // given
        List<String> list = new ArrayList<>();

        // when
        List<String> result = ArrayExample.removeDuplicates(list);

        // expect
        assertThat(result)
                .hasSize(0);
    }

    @Test
    void shouldReturnNullWhenGivenListIsNull() {
        // given
        List<String> list = null;

        // when
        List<String> result = ArrayExample.removeDuplicates(list);

        // expect
        assertThat(result).isNull();
    }
}