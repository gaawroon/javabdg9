package pl.sdacademy.gawronski.javabdg9.homeworks;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

import static org.assertj.core.api.Assertions.assertThat;

class PolishEmailValidatorTest {

//    @Test
//    void blednyTest(){
//        boolean result = EmailValidator.isValidEmail("@");
//        assertThat(result).isTrue();
//    }

    @Test
    void shouldHaveAtSign() {
        // given

        // when
        boolean result = PolishEmailValidator.isValidEmail("ab.pl");

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldHavePlPostfix() {
        // given

        // when
        boolean result = PolishEmailValidator.isValidEmail("valid@gmail.com");

        // then
        assertThat(result).isFalse();
    }

    @ParameterizedTest
    @NullAndEmptySource         // empty == ""
    void shouldNotBeEmpty(String email) {
        // given

        // when
        boolean result = PolishEmailValidator.isValidEmail(email);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldHaveSomethingBeforeAtSign() {
        // given

        // when
        boolean result = PolishEmailValidator.isValidEmail("@onet.pl");

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldHaveSomethingAfterAtSign() {
        // given

        // when
        boolean result = PolishEmailValidator.isValidEmail("valid@");

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldHaveDotAfterAtSign() {
        // given

        // when
        boolean result = PolishEmailValidator.isValidEmail("valid@onetpl");

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnTrue() {
        // given

        // when
        boolean result = PolishEmailValidator.isValidEmail("valid@onet.pl");

        // then
        assertThat(result).isTrue();
    }
}