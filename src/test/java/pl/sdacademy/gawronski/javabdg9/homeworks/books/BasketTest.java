package pl.sdacademy.gawronski.javabdg9.homeworks.books;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BasketTest {

    // [x] dodanie ksiazki do koszyka
    // [x] wyczyszczenie koszyka
    // [ ] pobranie wszystkich ksiazek w koszyku
    // [ ] suma cen wszystkich książek w koszyku

    @Mock
    List<Book> books;

    private Basket basket;

    @BeforeEach
    void setUp() {
        basket = new Basket(books);
    }

    @Test
    void shouldAddBooksToBasket() {
        // given
        Book book = new Book();
        book.setAuthor("Autor");
        book.setTitle("Tytul");
        book.setPrice(39.99f);

        when(books.add(book)).thenReturn(true);

        // when
        basket.addBook(book);

        // then
        verify(books, Mockito.times(1)).add(book);
    }

    @Test
    void shouldNotAddNullBook() {
        // given
        Book book = null;

        // when
        basket.addBook(book);

        // then
        verify(books, Mockito.times(0)).add(book);
    }

    @Test
    void shouldClearBasket() {
        // given
        doNothing().when(books).clear();        // nic nie rob, jesli wywolano books.clear()

        // when
        basket.clearBasket();

        // then
//        verify(books, Mockito.times(1)) == verify(books)
        verify(books).clear();
    }
}