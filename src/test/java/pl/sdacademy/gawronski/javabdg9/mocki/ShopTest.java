package pl.sdacademy.gawronski.javabdg9.mocki;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ShopTest {

//    @InjectMocks
    private Shop shop;

    @Mock
    ItemRepository itemRepository;

    @BeforeEach
    void setUp() {
        shop = new Shop(itemRepository);
    }

    @Test
    void shouldSumPricesOfTwoElements() {
        // given
        when(itemRepository.getItem("1")).thenReturn(new Item("1", "Laptop", 1050));
        when(itemRepository.getItem("2")).thenReturn(new Item("2", "Monitor", 500.99f));

        // when
        List<String> ids = new ArrayList<>();
        ids.add("1");
        ids.add("2");

        float price = shop.sumPrices(ids);

        // then
        assertThat(price).isEqualTo(1550.99f);

        verify(itemRepository, Mockito.times(1)).getItem("1");
        verify(itemRepository, Mockito.times(1)).getItem("2");

        verify(itemRepository, Mockito.times(2)).getItem(Mockito.anyString());
    }

    @Test
    void shouldSumPricesOfAllExistingItems() {
        // given
        when(itemRepository.getItem("1")).thenReturn(new Item("1", "Mysz", 29.99f));
        when(itemRepository.getItem("999")).thenThrow(new IllegalArgumentException());

        // when
        List<String> ids = new ArrayList<>();
        ids.add("1");
        ids.add("999");

        float price = shop.sumPrices(ids);

        // then
        assertThat(price).isEqualTo(29.99f);
    }
}